# Usage
Get diagnostic for specific language <br/>
`$ cargo run --bin i18n-check -- --lang <lang_code>` <br/>
Test all languages  <br/>
`$ cargo run --bin i18n-check --  --all`
Verify all directories  <br/>
`$ cargo run --bin i18n-check --  --verify`

